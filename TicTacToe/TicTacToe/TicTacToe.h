#pragma once

#include <iostream>
#include <conio.h>

class TicTacToe
{
private:

	//An array used to store the spaces of the game board. Once a player has Moved in one of the positions, the space should change to an X or an O.
	char m_board[9] = { '1', '2', '3', '4', '5', '6', '7', '8', '9' };

	//This will be used to detect when the board is full (all nine spaces are taken).
	int m_numTurns = 0;

	//This is used to track if it's X's turn or O's turn.
	char m_playerTurn;

	//This is used to check to see the winner of the game. A space should be used while the game is being played.
	char m_winner;

public:

	//Returns an 'X' or an 'O,' depending on which player's turn it is.
	char GetPlayerTurn()
	{
		if (m_numTurns == 1 ||
			m_numTurns == 3 ||
			m_numTurns == 5 ||
			m_numTurns == 7 ||
			m_numTurns == 9)
		{
			return 'X';
		}
		else
		{
			return 'O';
		}
	}

	bool IsOver()
	{
		//Returns true if the game is over (winner or tie), otherwise false if the game is still being played.
		if (m_numTurns > 9)
		{
			std::cout << "\n The game is a draw! \n";
			return true;
		}
		else
		{
			if (m_board[0] == m_board[1] && m_board[1] == m_board[2])
			{
				m_winner = m_board[0];
				DisplayResult();
				return true;
			}
			else if (m_board[0] == m_board[3] && m_board[3] == m_board[6])
			{
				m_winner = m_board[0];
				DisplayResult();
				return true;
			}
			else if (m_board[0] == m_board[4] && m_board[4] == m_board[8])
			{
				m_winner = m_board[0];
				DisplayResult();
				return true;
			}
			else if (m_board[3] == m_board[4] && m_board[4] == m_board[5])
			{
				m_winner = m_board[3];
				DisplayResult();
				return true;
			}
			else if (m_board[6] == m_board[7] && m_board[7] == m_board[8])
			{
				m_winner = m_board[6];
				DisplayResult();
				return true;
			}
			else if (m_board[1] == m_board[4] && m_board[4] == m_board[7])
			{
				m_winner = m_board[1];
				DisplayResult();
				return true;
			}
			else if (m_board[2] == m_board[5] && m_board[5] == m_board[8])
			{
				m_winner = m_board[2];
				DisplayResult();
				return true;
			}
			else if (m_board[2] == m_board[4] && m_board[4] == m_board[6])
			{
				m_winner = m_board[2];
				DisplayResult();
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	void DisplayBoard()
	{
		//This method will output the board to the console.
		std::cout << m_board[0] << " | " << m_board[1] << " | " << m_board[2] << "\n"
			<< "_________" << "\n" << "\n"
			<< m_board[3] << " | " << m_board[4] << " | " << m_board[5] << "\n"
			<< "_________" << "\n" << "\n"
			<< m_board[6] << " | " << m_board[7] << " | " << m_board[8] << "\n";

		m_numTurns++;
	}

	bool IsValidMove(int position)
	{
		//Param: An int (1 - 9) representing a square on the board. Returns true if the space on the board is open, otherwise false.
		if (m_board[position - 1] == 'X' || m_board[position - 1] == 'O')
		{
			return false;
		}
		else return true;
	}

	void Move(int position)
	{
		//Param: int (1 - 9) representing a square on the board. Places the current players character in the specified position on the board.
		m_board[position - 1] = GetPlayerTurn();
	}

	void DisplayResult()
	{
		//Displays a message stating who the winning player is, or if the game ended in a tie.
		std::cout << "\n" << m_winner << " wins!\n";
	}
};




